# ktor-hello-world

Kotlin Ktor hello world project

## Start locally

Once a JDK and gradle is installed, the following starts the service.

`gradle run`

To list all available targets, use `gradle tasks`.